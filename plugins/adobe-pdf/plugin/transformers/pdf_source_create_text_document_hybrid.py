import os
import tempfile

import numpy as np
from PIL import Image

from ingestum import transformers
from ingestum import sources
from ingestum import documents


class AdobeReader(transformers.pdf_source_create_text_document_hybrid.BaseReader):
    type: str = "adobe"

    def find_boxes(self, img):
        """Returns a list of tuples of text boxes in the given image using
        Adobe PDF Extract API. The tuples are in the form
        (x, y, width, height)."""

        tempdir = tempfile.TemporaryDirectory()
        pdf_path = os.path.join(tempdir.name, "page.pdf")
        pil_img = Image.fromarray(np.uint8(img)).convert("RGB")
        pil_img.save(pdf_path)
        pdf_source = sources.PDF(path=pdf_path)

        elements = transformers.pdf_source_create_text_document_adobe.Transformer(
            directory=tempdir.name
        ).get_elements(pdf_source)

        height = img.shape[0]
        boxes = [
            (
                int(e["Bounds"][0]),
                int(height - e["Bounds"][3]),
                int(e["Bounds"][2] - e["Bounds"][0]),
                int(e["Bounds"][3] - e["Bounds"][1]),
            )
            for e in elements
            if e["Type"] != "figure"
        ]
        # Figure bounding boxes are in e["attributes"]["BBox"]
        figure_boxes = [
            (
                int(e["attributes"]["BBox"][0]),
                int(height - e["attributes"]["BBox"][3]),
                int(e["attributes"]["BBox"][2] - e["attributes"]["BBox"][0]),
                int(e["attributes"]["BBox"][3] - e["attributes"]["BBox"][1]),
            )
            for e in elements
            if e["Type"] == "figure"
        ]

        boxes.extend(figure_boxes)

        tempdir.cleanup()
        return boxes


class Transformer(transformers.pdf_source_create_text_document_hybrid.Transformer):
    def transform(self, source: sources.PDF) -> documents.Text:
        return super().transform(source=source)
