# -*- coding: utf-8 -*-

#
# Copyright (c) 2021 Sorcero, Inc.
#
# This file is part of Sorcero's Language Intelligence platform
# (see https://www.sorcero.com).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#


import csv
import json
import os
import re
import shutil
import tempfile
import zipfile
from typing import Optional

from adobe.pdfservices.operation.auth.credentials import Credentials
from adobe.pdfservices.operation.execution_context import ExecutionContext
from adobe.pdfservices.operation.io.file_ref import FileRef
from adobe.pdfservices.operation.pdfops.extract_pdf_operation import ExtractPDFOperation
from adobe.pdfservices.operation.pdfops.options.extractpdf.extract_element_type import (
    ExtractElementType,
)
from adobe.pdfservices.operation.pdfops.options.extractpdf.extract_pdf_options import (
    ExtractPDFOptions,
)
from adobe.pdfservices.operation.pdfops.options.extractpdf.extract_renditions_element_type import (
    ExtractRenditionsElementType,
)
from adobe.pdfservices.operation.pdfops.options.extractpdf.table_structure_type import (
    TableStructureType,
)
from pydantic import BaseModel
from PyPDF2 import PdfFileReader, PdfFileWriter
from typing_extensions import Literal

from ingestum import documents
from ingestum import sources
from ingestum import transformers

__script__ = os.path.basename(__file__).replace(".py", "")


class LayoutMarkerConfig(BaseModel):
    title: Optional[str] = None
    header: Optional[str] = None
    table: Optional[str] = None
    figure: Optional[str] = None
    list_item: Optional[str] = None
    toc_item: Optional[str] = None
    footnote: Optional[str] = None
    paragraph: Optional[str] = None


class Transformer(transformers.base.BaseTransformer):
    """
    Transforms a `PDF` input source into a `Text` document where the Text
    document contains all human-readable text from the PDF, using the Adobe
    PDF Extract API.

    Requires ``INGESTUM_ADOBE_PDF_CLIENT_ID``,
    ``INGESTUM_ADOBE_PDF_CLIENT_SECRET``,
    ``INGESTUM_ADOBE_PDF_ORGANIZATION_ID``,
    ``INGESTUM_ADOBE_PDF_ACCOUNT_ID``, and ``INGESTUM_ADOBE_PDF_PRIVATE_KEY``
    environment variables for authentication.

    :param directory: Path to the directory to store images
    :type directory: str
    :param first_page: First page to be used
    :type first_page: int
    :param last_page: Last page to be used
    :type last_page: int
    :param layout_markers: Dictionary with layout markers to surround matching
        elements, e.g. ``TITLE_MARKER`` for titles or ``PARAGRAPH_MARKER`` for
        paragraphs.
    :type layout_markers: LayoutMarkerConfig
    """

    class ArgumentsModel(BaseModel):
        directory: str
        first_page: Optional[int] = None
        last_page: Optional[int] = None
        layout_markers: Optional[LayoutMarkerConfig] = None

    class InputsModel(BaseModel):
        source: sources.PDF

    class OutputsModel(BaseModel):
        document: documents.Text

    arguments: ArgumentsModel
    inputs: Optional[InputsModel]
    outputs: Optional[OutputsModel]

    type: Literal[__script__] = __script__

    def setup_credentials(self):
        """Creates an Adobe credentials instance with the given credentials."""

        client_id = os.environ["INGESTUM_ADOBE_PDF_CLIENT_ID"]
        client_secret = os.environ["INGESTUM_ADOBE_PDF_CLIENT_SECRET"]
        organization_id = os.environ["INGESTUM_ADOBE_PDF_ORGANIZATION_ID"]
        account_id = os.environ["INGESTUM_ADOBE_PDF_ACCOUNT_ID"]
        private_key = os.environ["INGESTUM_ADOBE_PDF_PRIVATE_KEY"]

        credentials = (
            Credentials.service_account_credentials_builder()
            .with_client_id(client_id)
            .with_client_secret(client_secret)
            .with_organization_id(organization_id)
            .with_account_id(account_id)
            .with_private_key(private_key)
            .build()
        )

        return credentials

    def extract_data(self, tempdir, source):
        """Passes the given source to the Adobe PDF Extract API and returns the
        JSON output as a dict."""

        pdf = PdfFileReader(source.path, "rb")
        crop = PdfFileWriter()

        first_page = self.arguments.first_page
        if first_page is None:
            first_page = 1

        last_page = self.arguments.last_page
        if last_page is None:
            last_page = pdf.getNumPages()

        for pageno in range(first_page - 1, last_page):
            p = pdf.getPage(pageno)
            crop.addPage(p)
        crop_path = os.path.join(tempdir.name, "crop.pdf")
        with open(crop_path, "wb") as f:
            crop.write(f)

        credentials = self.setup_credentials()

        # Create an ExecutionContext using credentials and create a new
        # operation instance.
        execution_context = ExecutionContext.create(credentials)
        extract_pdf_operation = ExtractPDFOperation.create_new()

        # Set operation input from a source file.
        source = FileRef.create_from_local_file(crop_path)
        extract_pdf_operation.set_input(source)

        # Build ExtractPDF options and set them into the operation
        extract_pdf_options: ExtractPDFOptions = (
            ExtractPDFOptions.builder()
            .with_elements_to_extract(
                [ExtractElementType.TEXT, ExtractElementType.TABLES]
            )
            .with_elements_to_extract_renditions(
                [
                    ExtractRenditionsElementType.TABLES,
                    ExtractRenditionsElementType.FIGURES,
                ]
            )
            .with_table_structure_format(TableStructureType.CSV)
            .build()
        )
        extract_pdf_operation.set_options(extract_pdf_options)

        # Execute the operation.
        result: FileRef = extract_pdf_operation.execute(execution_context)

        # Save the result to the output directory.
        shutil.move(result._file_path, os.path.join(tempdir.name, "output.zip"))

        # Extract the results from the output zip file.
        with zipfile.ZipFile(os.path.join(tempdir.name, "output.zip")) as zip_ref:
            zip_ref.extractall(tempdir.name)
        data = {}
        with open(os.path.join(tempdir.name, "structuredData.json"), "r") as f:
            data = json.loads(f.read())

        return data

    def add_element_types(self, elements):
        """Identifies the type of each element and adds it as an attribute."""

        # e.g. //Document/SomePath/Title
        TITLE = re.compile(r"/Title")

        # e.g. //Document/SomePath/H1
        HEADER = re.compile(r"/H\d+")

        # e.g. //Document/SomePath/Table[2]
        TABLE = re.compile(r"/Table(\[\d+])?")

        # e.g. //Document/SomePath/Figure[4]
        FIGURE = re.compile(r"/Figure(\[\d+])?")

        # e.g. //Document/SomePath/L/LI[3]
        LIST_ITEM = re.compile(r"/LI(\[\d+])?")

        # e.g. //Document/SomePath/TOC/TOCI[5]
        TOC_ITEM = re.compile(r"/TOCI(\[\d+])?")

        # e.g. //Document/SomePath/Footnote[2]
        FOOTNOTE = re.compile(r"/Footnote(\[\d+])?")

        # e.g. //Document/SomePath/P[4]
        PARAGRAPH = re.compile(r"/P(\[\d+])?")

        for element in elements:
            path = element["Path"]
            if TITLE.search(path):
                element["Type"] = "title"
            elif HEADER.search(path):
                element["Type"] = "header"
            elif TABLE.search(path):
                element["Type"] = "table"
            elif FIGURE.search(path):
                element["Type"] = "figure"
            elif LIST_ITEM.search(path):
                element["Type"] = "list_item"
            elif TOC_ITEM.search(path):
                element["Type"] = "toc_item"
            elif FOOTNOTE.search(path):
                element["Type"] = "footnote"
            elif PARAGRAPH.search(path):
                element["Type"] = "paragraph"
            else:
                element["Type"] = "other"

        return elements

    def process_tables(self, tempdir, elements):
        """Replaces table elements with a single table element containing
        the table in CSV format."""

        table_elements = [e for e in elements if e["Type"] == "table"]
        removed = []

        for table_element in table_elements:
            # The table parent element will have a filePaths key containing a
            # list of paths, with the first path pointing to the corresponding
            # CSV file.
            if "filePaths" in table_element:
                paths = [
                    path for path in table_element["filePaths"] if path.endswith(".csv")
                ]
                if len(paths) == 0:
                    continue
                csv_path = os.path.join(tempdir.name, paths[0])
                table = []
                with open(csv_path, encoding="utf-8-sig") as f:
                    reader = csv.reader(f)
                    for row in reader:
                        table.append(row)
                tabular_doc = documents.Tabular.new_from(None, content=table)
                tabular_doc = transformers.tabular_document_create_md_passage.Transformer().transform(
                    tabular_doc
                )
                table_element["Text"] = tabular_doc.dict()["content"]

                # Remove the table child elements
                base_path = table_element["Path"] + "/"
                child_elements = [
                    e
                    for e in table_elements
                    if e is not table_element and e["Path"].startswith(base_path)
                ]
                removed.extend(child_elements)

        processed_elements = [e for e in elements if e not in removed]
        return processed_elements

    def process_figures(self, tempdir, elements, pdf_height):
        """Replaces figure elements with text references to the images and
        saves the images to the given output directory."""

        figure_elements = [e for e in elements if e["Type"] == "figure"]
        removed = []

        for index, figure_element in enumerate(figure_elements):
            page = figure_element["Page"] + 1
            bbox = figure_element["attributes"]["BBox"]
            left = int(bbox[0])
            top = int(pdf_height - bbox[3])
            right = int(bbox[2])
            bottom = int(pdf_height - bbox[1])

            # Really thin or really short images are assumed to be page
            # artifacts or lines.
            MIN_IMG_WIDTH = 5
            MIN_IMG_HEIGHT = 5

            if right - left <= MIN_IMG_WIDTH or bottom - top <= MIN_IMG_HEIGHT:
                removed.append(figure_element)
                continue

            img_name = f"image.{index}.{page}.{left}.{top}.{right}.{bottom}"
            shutil.copy(
                os.path.join(tempdir.name, figure_element["filePaths"][0]),
                os.path.join(self.arguments.directory, img_name),
            )

            figure_element["Text"] = f"[file:///{img_name}]"

        processed_elements = [e for e in elements if e not in removed]
        return processed_elements

    def merge_paragraph_spans(self, elements):
        """Merges ParagraphSpan and Span elements from a single paragraph into
        a single element."""

        # e.g. //Document/SomePath/P/ParagraphSpan[2]
        PARAGRAPH_SPAN = re.compile(r"/(Paragraph)?Span(\[\d+])?")

        paragraph_spans = [e for e in elements if PARAGRAPH_SPAN.search(e["Path"])]
        removed = []

        for pspan in paragraph_spans:
            # The first ParagraphSpan will not have the [\d+]
            if PARAGRAPH_SPAN.search(pspan["Path"]).group(2) is None:
                matches = [
                    match
                    for match in paragraph_spans
                    if match is not pspan and match["Path"].startswith(pspan["Path"])
                ]
                pspan["Text"] += "".join([match["Text"] for match in matches])

                for match in matches:
                    if match["Page"] != pspan["Page"]:
                        del match["Text"]
                    else:
                        pspan["Bounds"] = [
                            min(match["Bounds"][0], pspan["Bounds"][0]),
                            min(match["Bounds"][1], pspan["Bounds"][1]),
                            max(match["Bounds"][2], pspan["Bounds"][2]),
                            max(match["Bounds"][3], pspan["Bounds"][3]),
                        ]
                        removed.append(match)

        merged_spans = [e for e in elements if e not in removed]
        return merged_spans

    def merge_list_items(self, elements):
        """Merges list item body and list item label elements into a single
        element, prepending each list item label to its corresponding list item
        body."""

        # e.g. //Document/SomePath/L/LI[2]/Lbl
        LIST_ITEM_LABEL = re.compile(r"/LI(\[\d+])?/Lbl")

        list_items = [e for e in elements if e["Type"] == "list_item"]
        removed = []

        for list_item in list_items:
            if LIST_ITEM_LABEL.search(list_item["Path"]):
                # Remove the "/Lbl" at the end to find the base path
                base_path = list_item["Path"][:-4]
                list_children = [
                    li
                    for li in list_items
                    if li is not list_item and li["Path"].startswith(base_path)
                ]
                if len(list_children) == 0:
                    continue
                list_body = list_children[0]
                list_body["Text"] = list_item["Text"] + list_body["Text"]
                list_body["Bounds"] = [
                    min(list_body["Bounds"][0], list_item["Bounds"][0]),
                    min(list_body["Bounds"][1], list_item["Bounds"][1]),
                    max(list_body["Bounds"][2], list_item["Bounds"][2]),
                    max(list_body["Bounds"][3], list_item["Bounds"][3]),
                ]
                removed.append(list_item)

        merged_list_items = [e for e in elements if e not in removed]
        return merged_list_items

    def get_element_text_with_markers(self, element):
        """Returns the text of the given element with the corresponding
        layout markers."""
        element_type = element["Type"]

        layout_markers = self.arguments.layout_markers
        if layout_markers is None:
            return element["Text"]

        if (
            element_type in layout_markers.dict()
            and layout_markers.dict()[element_type] is not None
        ):
            return (
                layout_markers.dict()[element_type]
                + element["Text"]
                + layout_markers.dict()[element_type]
            )

        return element["Text"]

    def get_elements(self, source):
        tempdir = tempfile.TemporaryDirectory()

        data = self.extract_data(tempdir, source)
        elements = data["elements"]
        pdf_height = data["pages"][0]["height"]

        elements = self.add_element_types(elements)
        elements = self.process_tables(tempdir, elements)
        elements = self.process_figures(tempdir, elements, pdf_height)
        elements = self.merge_paragraph_spans(elements)
        elements = self.merge_list_items(elements)

        tempdir.cleanup()
        return elements

    def extract(self, source):
        elements = self.get_elements(source)

        text = ""
        for element in elements:
            if "Text" in element:
                text += "\n" + self.get_element_text_with_markers(element) + "\n"

        return text.strip()

    def transform(self, source: sources.PDF) -> documents.Text:
        super().transform(source=source)

        return documents.Text.new_from(source, content=self.extract(source))
