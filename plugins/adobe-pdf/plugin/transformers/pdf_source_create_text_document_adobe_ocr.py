# -*- coding: utf-8 -*-

#
# Copyright (c) 2021 Sorcero, Inc.
#
# This file is part of Sorcero's Language Intelligence platform
# (see https://www.sorcero.com).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#


import logging
import os
import shutil
import tempfile
from typing import Optional

import numpy as np
from pdf2image import convert_from_path
from pdfminer.pdfpage import PDFPage
from PIL import ImageOps

from pydantic import BaseModel
from typing_extensions import Literal

from ingestum import documents
from ingestum import sources
from ingestum import transformers
from ingestum import utils
from .pdf_source_create_text_document_adobe import LayoutMarkerConfig
from .pdf_source_create_text_document_adobe import Transformer as TTransformer

__logger__ = logging.getLogger("ingestum")
__script__ = os.path.basename(__file__).replace(".py", "")


class Transformer(transformers.base.BaseTransformer):
    """
    Transforms a `PDF` input source into a `Text` document where the Text
    document contains all human-readable text from the PDF, using the Adobe
    PDF Extract API.

    This variant supports documents with images of text and scanned PDFs.

    Requires ``INGESTUM_ADOBE_PDF_CLIENT_ID``,
    ``INGESTUM_ADOBE_PDF_CLIENT_SECRET``,
    ``INGESTUM_ADOBE_PDF_ORGANIZATION_ID``,
    ``INGESTUM_ADOBE_PDF_ACCOUNT_ID``, and ``INGESTUM_ADOBE_PDF_PRIVATE_KEY``
    environment variables for authentication for Adobe PDF Extract API.

    Requires ``INGESTUM_AZURE_CV_ENDPOINT`` and
    ``INGESTUM_AZURE_CV_SUBSCRIPTION_KEY`` environment variables for
    authentication if using Azure OCR.

    :param directory: Path to the directory to store images
    :type directory: str
    :param first_page: First page to be used
    :type first_page: int
    :param last_page: Last page to be used
    :type last_page: int
    :param layout_markers: Dictionary with layout markers to surround matching
        elements, e.g. ``TITLE_MARKER`` for titles or ``PARAGRAPH_MARKER`` for
        paragraphs.
    :type layout_markers: LayoutMarkerConfig
    :param engine: The OCR engine to use, ``pytesseract`` (default) or
        ``azure``.
    :type engine: str
    """

    class ArgumentsModel(BaseModel):
        directory: str
        first_page: Optional[int] = None
        last_page: Optional[int] = None
        layout_markers: Optional[LayoutMarkerConfig] = None
        engine: Optional[str] = "pytesseract"

    class InputsModel(BaseModel):
        source: sources.PDF

    class OutputsModel(BaseModel):
        document: documents.Text

    arguments: ArgumentsModel
    inputs: Optional[InputsModel]
    outputs: Optional[OutputsModel]

    type: Literal[__script__] = __script__

    @staticmethod
    def get_size(source):
        pdf = open(source.path, "rb")

        pages = PDFPage.get_pages(
            pdf, pagenos=None, caching=True, check_extractable=False
        )
        page = list(pages)[0]

        width = page.mediabox[2]
        height = page.mediabox[3]

        pdf.close()
        return (width, height)

    def process_figure(self, figure, pdf_width_scaler, pdf_height_scaler):
        """Converts figures named with image coordinates to PDF coordinates."""
        img_filename = figure["Text"].split("file:///")[1][:-1]
        coords = img_filename.split(".")
        coords[3] = str(int(int(coords[3]) * pdf_width_scaler))  # left
        coords[4] = str(int(int(coords[4]) * pdf_height_scaler))  # top
        coords[5] = str(int(int(coords[5]) * pdf_width_scaler))  # right
        coords[6] = str(int(int(coords[6]) * pdf_height_scaler))  # bottom

        new_filename = ".".join(coords)
        shutil.move(
            os.path.join(self.arguments.directory, img_filename),
            os.path.join(self.arguments.directory, new_filename),
        )
        figure["Text"] = f"[file:///{new_filename}]"

    def process_table(self, tempdir, table, page_img, img_height):
        """Extracts the content of the given table using OCR and formats it in
        markdown. If no table is found, returns False, True otherwise."""
        bbox = (
            int(table["Bounds"][0]),
            int(img_height - table["Bounds"][3]),
            int(table["Bounds"][2]),
            int(img_height - table["Bounds"][1]),
        )

        crop = page_img.crop(bbox)
        crop = ImageOps.expand(crop, border=2, fill="white")
        crop = ImageOps.expand(crop, border=1)
        crop = ImageOps.expand(crop, border=2, fill="white")
        crop_path = os.path.join(tempdir.name, "crop.png")
        crop.save(crop_path)
        img_source = sources.Image(path=crop_path)

        table_detected = False
        try:
            tabular_doc = transformers.image_source_create_tabular_document.Transformer().transform(
                img_source
            )
            tabular_doc = (
                transformers.tabular_document_create_md_passage.Transformer().transform(
                    tabular_doc
                )
            )
            table_content = tabular_doc.dict()["content"]
            table["Text"] = table_content
            table_detected = True
        except ValueError:
            # XXX Camelot returns a ValueError if no table is
            # detected. We treat the element as text instead.
            pass

        return table_detected

    def extract(self, source):
        tempdir = tempfile.TemporaryDirectory()

        # Convert all of the pages to images in order to force the Adobe PDF
        # Extract API to detect layout with OCR.
        page_imgs = convert_from_path(
            source.path,
            output_folder=tempdir.name,
        )

        first_page = page_imgs[0]
        other_pages = page_imgs[1:]

        pdf_path = os.path.join(tempdir.name, os.path.basename(source.path))
        first_page.save(pdf_path, save_all=True, append_images=other_pages)
        pdf_source = sources.PDF(path=pdf_path)

        transformer_adobe = TTransformer(
            directory=self.arguments.directory,
            first_page=self.arguments.first_page,
            last_page=self.arguments.last_page,
            layout_markers=self.arguments.layout_markers,
        )
        elements = transformer_adobe.get_elements(pdf_source)

        # Import all OCR engine classes, including plugins
        engine_classes = utils.find_subclasses(
            transformers.pdf_source_create_text_document_ocr.BaseEngine
        )
        Engine = next(
            (
                e
                for e in engine_classes
                if e.__fields__["type"].default == self.arguments.engine
            ),
            None,
        )
        if Engine is None:
            raise ValueError(f"{self.arguments.engine} does not exist")
        e = Engine()

        pdf_width, pdf_height = self.get_size(source)
        img_width, img_height = page_imgs[0].size
        pdf_width_scaler = pdf_width / float(img_width)
        pdf_height_scaler = pdf_height / float(img_height)

        # Combine Adobe layout elements with OCR text
        text = ""
        for pageno, page_img in enumerate(page_imgs):
            layout_elements = [e for e in elements if e["Page"] == pageno]
            if len(layout_elements) == 0:
                continue

            ocr_elements = e.read(
                np.array(page_img), [0, 0, img_width, img_height], pdf_width, pdf_height
            )

            for ocr_element in ocr_elements:
                ocr_element["left"] /= pdf_width_scaler
                ocr_element["right"] /= pdf_width_scaler
                ocr_element["top"] /= pdf_height_scaler
                ocr_element["bottom"] /= pdf_height_scaler

            BOUNDING_BOX_TOLERANCE = 10
            for layout_element in layout_elements:
                if layout_element["Type"] == "figure":
                    self.process_figure(
                        layout_element, pdf_width_scaler, pdf_height_scaler
                    )
                    continue

                if layout_element["Type"] == "table":
                    if self.process_table(
                        tempdir, layout_element, page_img, img_height
                    ):
                        continue
                    else:
                        layout_element["Type"] = "paragraph"

                bounded_elements = [
                    e
                    for e in ocr_elements
                    if e["left"] + BOUNDING_BOX_TOLERANCE >= layout_element["Bounds"][0]
                    and e["right"] - BOUNDING_BOX_TOLERANCE
                    <= layout_element["Bounds"][2]
                    and img_height - e["top"] - BOUNDING_BOX_TOLERANCE
                    <= layout_element["Bounds"][3]
                    and img_height - e["bottom"] + BOUNDING_BOX_TOLERANCE
                    >= layout_element["Bounds"][1]
                ]

                text_elements = [e["text"] for e in bounded_elements]
                layout_element["Text"] = " ".join(text_elements)

            for element in layout_elements:
                if "Text" in element:
                    text += (
                        "\n"
                        + transformer_adobe.get_element_text_with_markers(element)
                        + "\n"
                    )

        return text.strip()

    def transform(self, source: sources.PDF) -> documents.Text:
        super().transform(source=source)

        return documents.Text.new_from(source, content=self.extract(source))
