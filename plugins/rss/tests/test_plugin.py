# -*- coding: utf-8 -*-

#
# Copyright (c) 2021 Sorcero, Inc.
#
# This file is part of Sorcero's Language Intelligence platform
# (see https://www.sorcero.com).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from ingestum import engine
from ingestum import manifests
from ingestum import pipelines


def run_pipeline(pipeline, source):
    results, *_ = engine.run(
        manifest=manifests.Base(sources=[source]),
        pipelines=[pipeline],
        pipelines_dir=None,
        artifacts_dir=None,
        workspace_dir=None,
    )

    return results[0]


def test_pipeline_rss():
    pipeline = pipelines.Base.parse_file(
        "plugins/rss/tests/pipelines/pipeline_rss.json"
    )
    source = manifests.sources.RSS(
        id="",
        pipeline=pipeline.name,
        url="https://blogs.gnome.org/tchx84/feed/",
        destination=manifests.sources.destinations.Void(),
    )
    # test that all the plugin components can be de-serialized and can run
    run_pipeline(pipeline, source)
