#
# Copyright (c) 2021 Sorcero, Inc.
#
# This file is part of Sorcero's Language Intelligence platform
# (see https://www.sorcero.com).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

#
# Copyright (c) 2021 Sorcero, Inc.
#
# This file is part of Sorcero's Language Intelligence platform
# (see https://www.sorcero.com).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#


from ingestum import transformers
from ingestum import sources
from ingestum import documents
from ingestum import utils

import cv2
import os
import re

import layoutparser as lp

from tempfile import TemporaryDirectory
from pdf2image import convert_from_path

__script__ = os.path.basename(__file__).replace(".py", "")


class LPTitleParser(
    transformers.pdf_source_create_publication_document.BaseTitleParser
):
    priority = 4

    def get_title(self, source):
        directory = TemporaryDirectory()

        image_path = convert_from_path(
            pdf_path=source.path,
            output_folder=directory.name,
            first_page=1,
            last_page=1,
            paths_only=True,
        )[0]
        image = cv2.imread(image_path)[..., ::-1]

        directory.cleanup()

        model = lp.Detectron2LayoutModel(
            "lp://PubLayNet/faster_rcnn_R_50_FPN_3x/config",
            extra_config=["MODEL.ROI_HEADS.SCORE_THRESH_TEST", 0.8],
            label_map={0: "Text", 1: "Title", 2: "List", 3: "Table", 4: "Figure"},
        )

        blocks = model.detect(image)
        h, w = image.shape[:2]
        left_interval = lp.Interval(0, w / 2 * 1.05, axis="x").put_on_canvas(image)

        ocr_agent = lp.TesseractAgent(languages="eng")

        title_blocks = lp.Layout([b for b in blocks if b.type == "Title"])

        if len(title_blocks) > 0:
            left_blocks = title_blocks.filter_by(left_interval, center=True)
            left_blocks = sorted(left_blocks, key=lambda b: b.coordinates[1])

            right_blocks = [b for b in title_blocks if b not in left_blocks]
            right_blocks = sorted(right_blocks, key=lambda b: b.coordinates[1])

            title_blocks = lp.Layout(
                [b.set(id=idx) for idx, b in enumerate(left_blocks + right_blocks)]
            )

            enriched_blocks = []

            for block in title_blocks:
                segment_image = block.crop_image(image)
                text = (
                    ocr_agent.detect(segment_image)
                    .replace("\n", " ")
                    .replace(" \x0c", "")
                )
                word_count = len(text.split())

                if word_count < 6 or word_count > 19:
                    continue
                block.set(text=text, inplace=True)
                enriched_blocks.append(
                    {"height": block.height, "text": text, "score": block.score}
                )

            if len(enriched_blocks) > 0:
                enriched_blocks.sort(
                    key=lambda block: (block["height"], block["score"]), reverse=True
                )
                return utils.sanitize_string(
                    re.sub(" +", " ", enriched_blocks[0]["text"])
                )

        # If the 'title' blocks were too short/long, maybe they don't really contain the
        # title (Example: RM - Myopericarditis following mRNA COVID-19 Vaccination in
        # Adolescents 12 through 18 Years of Age)
        text_blocks = lp.Layout([b for b in blocks if b.type == "Text"])

        if len(text_blocks) > 0:
            left_blocks = text_blocks.filter_by(left_interval, center=True)
            left_blocks = sorted(text_blocks, key=lambda b: b.coordinates[1])

            right_blocks = [b for b in text_blocks if b not in left_blocks]
            right_blocks = sorted(text_blocks, key=lambda b: b.coordinates[1])

            text_blocks = lp.Layout(
                [b.set(id=idx) for idx, b in enumerate(left_blocks + right_blocks)]
            )

            enriched_blocks = []

            for block in text_blocks:
                segment_image = block.crop_image(image)
                text = (
                    ocr_agent.detect(segment_image)
                    .replace("\n", " ")
                    .replace(" \x0c", "")
                )
                word_count = len(text.split())

                if word_count < 6 or word_count > 19:
                    continue
                block.set(text=text, inplace=True)
                enriched_blocks.append(
                    {"height": block.height, "text": text, "score": block.score}
                )

            if len(enriched_blocks) > 0:
                enriched_blocks.sort(
                    key=lambda block: (block["height"], block["score"]), reverse=True
                )
                return utils.sanitize_string(
                    re.sub(" +", " ", enriched_blocks[0]["text"])
                )

        return None


class Transformer(transformers.pdf_source_create_publication_document.Transformer):
    def transform(self, source: sources.PDF) -> documents.Publication:
        return super().transform(source)
