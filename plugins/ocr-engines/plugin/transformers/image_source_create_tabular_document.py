from ingestum import transformers
from ingestum import sources
from ingestum import documents

import easyocr


class EasyOCREngine(transformers.image_source_create_tabular_document.BaseEngine):
    class Config:
        extra = "allow"

    type: str = "easyocr"

    def __init__(self):
        super().__init__()
        self._reader = easyocr.Reader(["en"])

    def read(self, img, rect):
        """Returns the text in the given cell using EasyOCR."""
        x, y, width, height = rect
        crop = img[y : y + height, x : x + width]
        output = self._reader.readtext(crop, detail=0)
        text = " ".join(output)
        text = text.replace("\n", " ")
        text = text.strip()

        return text


class Transformer(transformers.image_source_create_tabular_document.Transformer):
    def transform(self, source: sources.Image) -> documents.Tabular:
        return super().transform(source=source)
